﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Module4 module = new Module4();
            module.Task_2(new int[] { 1, 2, 3 }, new int[] { -1, 12, 23, 94, -65 });
        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            return array.Max();
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            return array.Min();
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            return array.Max() - array.Min();
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            int max = array.Max();
            int min = array.Min();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = (i % 2 == 0) ? array[i] + max : array[i] - min;
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] biggerArray, smallerArray;
            biggerArray = a.Length > b.Length ? a : b;
            smallerArray = a.Length > b.Length ? b : a;
            for (int i = 0; i < biggerArray.Length; i++)
            {
                if (i < smallerArray.Length)
                {
                    biggerArray[i] += smallerArray[i];
                }
            }
            return biggerArray;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException("Radius is negative!");
            }
            length = 2 * Math.PI * radius;
            square = Math.PI * radius * radius;
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem  = array.Max();
            minItem = array.Min();
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }
            sumOfItems = sum;

        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException("Radius is negative!");
            }
            var tuple = (2 * Math.PI * radius, Math.PI * radius * radius);
            return tuple;
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }

            var tuple = (array.Min(), array.Max(), sum);
            return tuple;
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }

            Array.Sort(array);

            if (direction == SortDirection.Descending)
            {
                Array.Reverse(array);
            }
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            throw new NotImplementedException();
        }
    }
}
